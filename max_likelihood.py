# Auxiliary code for assignment 2
# Maximum likelihood estimation for the Gamma distribution
# (when the two parameters are unknown)
# Javier Amezcua, February 2015

import numpy as np
import scipy.special as scsf
from scipy.optimize import fsolve

def ml_gamma(x):
    logx = np.log(x)
    x_bar = np.mean(x)
    logx_bar = np.mean(logx)

    def func_alpha(alpha_dum):
        return scsf.digamma(alpha_dum)-np.log(alpha_dum)-logx_bar+np.log(x_bar)

    first_guess = 1
    alpha = fsolve(func_alpha, first_guess)
    beta = x_bar/alpha    

    return alpha,beta












